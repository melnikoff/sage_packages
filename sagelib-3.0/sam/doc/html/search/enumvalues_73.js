var searchData=
[
  ['sac_5ferror',['SAC_ERROR',['../namespacesam.html#ac7a8ebf3f69465e39bce8550eed0722ba96985ee2537ec5be6e32075ffe005f1e',1,'sam']]],
  ['sac_5fnot_5fregistered',['SAC_NOT_REGISTERED',['../namespacesam.html#ac7a8ebf3f69465e39bce8550eed0722ba7af1c5c7da31eb60ee8a751b258f5da3',1,'sam']]],
  ['sac_5fosc_5ferror',['SAC_OSC_ERROR',['../namespacesam.html#ac7a8ebf3f69465e39bce8550eed0722ba340b81c6fc2da62438fcce11a2bb9ea4',1,'sam']]],
  ['sac_5frequest_5fdenied',['SAC_REQUEST_DENIED',['../namespacesam.html#ac7a8ebf3f69465e39bce8550eed0722bab403ee2b6d4c1b336baa757d47f65716',1,'sam']]],
  ['sac_5fsuccess',['SAC_SUCCESS',['../namespacesam.html#ac7a8ebf3f69465e39bce8550eed0722ba1bcfe61283dc40245deba6dfd3ed4ab8',1,'sam']]],
  ['sac_5ftimeout',['SAC_TIMEOUT',['../namespacesam.html#ac7a8ebf3f69465e39bce8550eed0722baffb33b31a57957440251c5529c2d0e82',1,'sam']]],
  ['sam_5ferr_5fdefault',['SAM_ERR_DEFAULT',['../namespacesam.html#aaaf18c96090e2f3dda803fb694a3ea00a1a0a38fe42669af34462b1036a90245b',1,'sam']]],
  ['sam_5ferr_5finvalid_5fid',['SAM_ERR_INVALID_ID',['../namespacesam.html#aaaf18c96090e2f3dda803fb694a3ea00a1b90cb570b8eefac03fb3eace6c43550',1,'sam']]],
  ['sam_5ferr_5finvalid_5ftype',['SAM_ERR_INVALID_TYPE',['../namespacesam.html#aaaf18c96090e2f3dda803fb694a3ea00a43d20d56980c3ad3352c58490d3dbb94',1,'sam']]],
  ['sam_5ferr_5fmax_5fclients',['SAM_ERR_MAX_CLIENTS',['../namespacesam.html#aaaf18c96090e2f3dda803fb694a3ea00a0872cf8f83acbfd746548c181a1592ab',1,'sam']]],
  ['sam_5ferr_5fno_5ffree_5foutput',['SAM_ERR_NO_FREE_OUTPUT',['../namespacesam.html#aaaf18c96090e2f3dda803fb694a3ea00ae303a41568d4d691ec0d7a4e15aa1b80',1,'sam']]],
  ['sam_5ferr_5fversion_5fmismatch',['SAM_ERR_VERSION_MISMATCH',['../namespacesam.html#aaaf18c96090e2f3dda803fb694a3ea00a96e9ec536beff0d134028c2ed2f1dbd2',1,'sam']]]
];
